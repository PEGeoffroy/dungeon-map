"use strict";

let main = document.getElementById("main");

function displayRoom(array) {
    main.innerHTML = "";
    let room = document.createElement("div");
    room.classList.add("room");

    for (let i = 0; i < array.length; i++) {
        let row = document.createElement("div");
        row.classList.add("row");
        for (let j = 0; j < array[i].length; j++) {
            let cell = document.createElement("div");
            cell.classList.add("cell");
            cell.dataset.x = i;
            cell.dataset.y = j;
            if (i % 2 === 0) {
                if (j % 2 === 0) {
                    cell.classList.add("column");
                } else {
                    cell.classList.add("h-wall");
                }
            } else {
                if (j % 2 === 0) {
                    cell.classList.add("v-wall");
                } else {
                    cell.classList.add("heart");
                }
            }
            if (array[i][j] === "c") {
                cell.classList.add("colored");
            } else if (array[i][j] === "d") {
                cell.classList.add("door");
            }
            row.appendChild(cell);
        }
        room.appendChild(row);
    }
    main.appendChild(room);

    let cells = document.getElementsByClassName("cell");

    for (let i = 0; i < cells.length; i++) {
        cells[i].addEventListener("click", () => {
            if (cells[i].classList.contains("colored")) {
                cells[i].classList.remove("colored");
                array[cells[i].dataset.x][cells[i].dataset.y] = "e";
            } else if (cells[i].classList.contains("door")) {
                array[cells[i].dataset.x][cells[i].dataset.y] = "c";
                cells[i].classList.add("colored");
                cells[i].classList.remove("door");
            } else {
                array[cells[i].dataset.x][cells[i].dataset.y] = "c";
                cells[i].classList.add("colored");
            }
            // Test the neighbors of the left and right columns
            if (cells[i].classList.contains("h-wall") && cells[i].classList.contains("colored")) {
                let neighborsLeft = 0;
                let neighborsRight = 0;

                // Test the 4 walls on the left
                if (cells[i].dataset.x !== "0" &&
                    array[parseInt(cells[i].dataset.x) - 1][parseInt(cells[i].dataset.y) - 1] === "c"
                ) neighborsLeft++;
                if (cells[i].dataset.x < array.length - 1 &&
                    array[parseInt(cells[i].dataset.x) + 1][parseInt(cells[i].dataset.y) - 1] === "c"
                ) neighborsLeft++;
                if (array[parseInt(cells[i].dataset.x)][parseInt(cells[i].dataset.y) - 2] === "c") neighborsLeft++;
                    
                if (neighborsLeft >= 1) array[parseInt(cells[i].dataset.x)][parseInt(cells[i].dataset.y) - 1] = "c";

                // Test the 4 walls on the right
                if (cells[i].dataset.x !== "0" &&
                    array[parseInt(cells[i].dataset.x) - 1][parseInt(cells[i].dataset.y) + 1] === "c"
                ) neighborsRight++;
                if (cells[i].dataset.x < array.length - 1 &&
                    array[parseInt(cells[i].dataset.x) + 1][parseInt(cells[i].dataset.y) + 1] === "c"
                ) neighborsRight++;
                if (array[parseInt(cells[i].dataset.x)][parseInt(cells[i].dataset.y) + 2] === "c") neighborsRight++;

                if (neighborsRight >= 1) array[parseInt(cells[i].dataset.x)][parseInt(cells[i].dataset.y) + 1] = "c";
            }
            // Test the neighbors of the top and bottom columns
            if (cells[i].classList.contains("v-wall") && cells[i].classList.contains("colored")) {
                let neighborsTop = 0;
                let neighborsBottom = 0;

                // Test the top 4 walls
                if (cells[i].dataset.y !== "0" &&
                    array[parseInt(cells[i].dataset.x) - 1][parseInt(cells[i].dataset.y) - 1] === "c"
                ) neighborsTop++;
                if (cells[i].dataset.y < array[0].length - 1 &&
                    array[parseInt(cells[i].dataset.x) - 1][parseInt(cells[i].dataset.y) + 1] === "c"
                ) neighborsTop++;
                if (cells[i].dataset.x !== "1" &&
                    array[parseInt(cells[i].dataset.x) - 2][parseInt(cells[i].dataset.y)] === "c"
                ) neighborsTop++;
                    
                if (neighborsTop >= 1) array[parseInt(cells[i].dataset.x) - 1][parseInt(cells[i].dataset.y)] = "c";

                // Test the 4 bottom walls
                if (cells[i].dataset.y !== "0" &&
                    array[parseInt(cells[i].dataset.x) + 1][parseInt(cells[i].dataset.y) - 1] === "c"
                ) neighborsBottom++;
                if (cells[i].dataset.y < array[0].length - 1 &&
                    array[parseInt(cells[i].dataset.x) + 1][parseInt(cells[i].dataset.y) + 1] === "c"
                ) neighborsBottom++;
                if (cells[i].dataset.x < array.length - 2 &&
                    array[parseInt(cells[i].dataset.x) + 2][parseInt(cells[i].dataset.y)] === "c"
                ) neighborsBottom++;

                if (neighborsBottom >= 1) array[parseInt(cells[i].dataset.x) + 1][parseInt(cells[i].dataset.y)] = "c";
            }
            displayRoom(array);
        });
        cells[i].addEventListener('contextmenu', e => {
            e.preventDefault();
            if (cells[i].classList.contains("door")) {
                array[cells[i].dataset.x][cells[i].dataset.y] = "e";
                cells[i].classList.remove("door");
            } else if (cells[i].classList.contains("colored")) {
                array[cells[i].dataset.x][cells[i].dataset.y] = "d";
                cells[i].classList.add("door");
                cells[i].classList.remove("colored");
            } else {
                array[cells[i].dataset.x][cells[i].dataset.y] = "d";
                cells[i].classList.add("door");
            }
            // consoleRoom(array);
            // displayRoom(array);
        });
    }
}

function createRoom(height, width = height) {
    let result = [];
    let roomHeight = height * 2 + 1;
    let roomWidth = width * 2 + 1;

    for (let i = 0; i < roomHeight; i++) {
        result.push([]);
        for (let j = 0; j < roomWidth; j++) {
            result[i].push("e");
        }
    }
    displayRoom(result);
    // consoleRoom(result);
    return result;
}

function consoleRoom(array) {
    console.log("---");
    array.forEach(row => {
        let string = "";
        row.forEach(cell => {
            string = string.concat(cell);
        });
        console.log(string);
    });
}

function checkNeighbors(x, y) {
}

let roomTest = createRoom(10, 7);
